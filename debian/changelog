liblocale-subcountry-perl (2.07-1) unstable; urgency=medium

  * Import upstream version 2.07.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Tue, 26 Oct 2021 22:22:26 +0200

liblocale-subcountry-perl (2.06-1) unstable; urgency=medium

  * Import upstream version 2.06.
  * Update debian/upstream/metadata.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Mon, 18 Jan 2021 21:49:48 +0100

liblocale-subcountry-perl (2.05-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 2.05.
  * Update years of upstream and packaging copyright.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.0.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Aug 2019 23:22:36 +0200

liblocale-subcountry-perl (2.04-1) unstable; urgency=medium

  * Import upstream version 2.04.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.1.3.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Jan 2018 00:02:00 +0100

liblocale-subcountry-perl (2.02-1) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * Remove Marc 'HE' Brockschmidt from Uploaders. (Closes: #873353)

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Damyan Ivanov ]
  * New upstream version 2.02
  * update years of upstream copyright
  * declare conformance with Policy 4.1.2 (no changes)

 -- Damyan Ivanov <dmn@debian.org>  Sun, 10 Dec 2017 18:51:34 +0000

liblocale-subcountry-perl (2.01-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Nuno Carvalho ]
  * New upstream release.
  * d/control: add new dependency libjson-perl.

  [ gregor herrmann ]
  * Drop libtest-pod* from Build-Depends-Indep.
    The respective tests are not run during build anymore.
  * Add note about potentially incompatible changes to debian/NEWS.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 26 Aug 2016 16:28:37 +0200

liblocale-subcountry-perl (1.66-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.66
  * d/u/metadata: add Repository data
  * Update years of upstream copyright

 -- Lucas Kanashiro <kanashiro@debian.org>  Sun, 24 Jul 2016 23:45:53 -0300

liblocale-subcountry-perl (1.65-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 1.65
  * Update Debian packaging copyright
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.8

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 13 Jun 2016 16:11:01 -0300

liblocale-subcountry-perl (1.64-1) unstable; urgency=medium

  * New upstream release.
  * Update years of upstream and packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 04 Jul 2015 23:07:01 +0200

liblocale-subcountry-perl (1.63-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Declare the package autopkgtestable
  * Update to Standards-Version 3.9.6
  * Add explicit build dependency on libmodule-build-perl

 -- Niko Tyni <ntyni@debian.org>  Sat, 06 Jun 2015 11:04:04 +0300

liblocale-subcountry-perl (1.63-1) unstable; urgency=medium

  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Update years of copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Wed, 07 May 2014 20:17:48 +0200

liblocale-subcountry-perl (1.62-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Oct 2013 17:19:26 +0200

liblocale-subcountry-perl (1.61-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * Drop package.patch, issue fixed upstream.
  * debian/copyright: update years.

 -- gregor herrmann <gregoa@debian.org>  Sun, 17 Feb 2013 18:59:57 +0100

liblocale-subcountry-perl (1.56-2) unstable; urgency=low

  * Add patch package.patch from Ivan Kohler: add a package declaration.
    (Closes: #688069)
  * Bump Standards-Version to 3.9.4 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Tue, 30 Oct 2012 17:44:18 +0100

liblocale-subcountry-perl (1.56-1) unstable; urgency=low

  * Team upload.
  * d/control: Wrap line.
  * New upstream release.

 -- Nuno Carvalho <smash@cpan.org>  Wed, 08 Aug 2012 14:12:00 +0100

liblocale-subcountry-perl (1.50-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release.
  * Update years of copyright.
  * debian/copyright: update to Copyright-Format 1.0.
  * Bump Standards-Version to 3.9.3 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 26 May 2012 20:56:43 +0200

liblocale-subcountry-perl (1.47-1) unstable; urgency=low

  * Team upload.
  * New upstream release

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 30 Apr 2011 22:14:37 +0200

liblocale-subcountry-perl (1.46-1) unstable; urgency=low

  * New upstream release.
  * Add debian/NEWS.
  * Set Standards-Version to 3.9.2 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Apr 2011 20:07:08 +0200

liblocale-subcountry-perl (1.44-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Jonathan Yu ]
  * New upstream release
  * Rewrite control description
  * Bump debhelper compat to 8
  * Use new 3.0 (quilt) source format
  * Use short debhelper rules format
  * Standards-Version 3.9.1 (no changes)
  * Refresh copyright information
  * Add myself to Uploaders and Copyright
  * Email change: Niko Tyni -> ntyni@debian.org

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 07 Mar 2011 22:05:17 -0500

liblocale-subcountry-perl (1.41-1) unstable; urgency=low

  * New upstream release.
  * Refresh debian/rules, no functional changes; don't install README any
    more (text version of the inline POD).
  * debian/control:
    - add /me to Uploaders
    - wrap long line
    - mention module name in long description

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Apr 2008 19:45:49 +0200

liblocale-subcountry-perl (1.40-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream release.
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * Set Standards-Version to 3.7.3 (no changes needed).
  * debian/watch: use dist-based URL.
  * debian/copyright: update year of copyright, add actual upstream source
    location instead of pointing to CPAN at large.
  * debian/rules:
    - remove compiler flags, we don't compile anything
    - move tests to build-stamp target
    - move dh_clean before make distclean
    - remove empty /usr/lib/perl5 directory only if it exists
    - remove unneeded dh_* calls
  * Bump debhelper compatibility level to 6.
  * Remove debian/liblocale-subcountry-perl.docs, install README directly
    from debian/rules.

 -- Roberto C. Sanchez <roberto@debian.org>  Sun, 13 Jan 2008 15:13:33 -0500

liblocale-subcountry-perl (1.38-2) unstable; urgency=low

  [ Frank Lichtenheld ]
  * Fix typo in Description

  [ Joey Hess ]
  * Add myself to uploaders.
  * Remove sub-revision from standards-version field.

 -- Joey Hess <joeyh@debian.org>  Sat, 11 Aug 2007 22:05:31 -0400

liblocale-subcountry-perl (1.38-1) unstable; urgency=low

  * New upstream release
  * debian/control:
   + Added me to Uploaders
   + Standards-Version: upgraded to 3.7.2.1
   + Build-Depends: debhelper upgraded to (>= 5.1)
  * debian/compat: upgraded to 5

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri,  1 Dec 2006 17:12:17 +0100

liblocale-subcountry-perl (1.37-1) unstable; urgency=low

  * New upstream release.
  * Move debhelper from Build-Depends-Indep to Build-Depends,
    as it's required for the 'clean' target.
  * Don't ignore the result of 'make clean'.
  * Update debian/copyright: the module is nowadays licensed
    as Perl itself.
  * Add libtest-pod-perl and libtest-pod-coverage-perl to Build-Depends-Indep.

 -- Niko Tyni <ntyni@iki.fi>  Tue, 18 Apr 2006 00:26:42 +0300

liblocale-subcountry-perl (1.36-1) unstable; urgency=low

  * New upstream release

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 12 Jul 2005 13:08:14 +0300

liblocale-subcountry-perl (1.34-1) unstable; urgency=low

  * New upstream release

 -- Gunnar Wolf <gwolf@debian.org>  Mon, 26 Apr 2004 18:25:19 -0500

liblocale-subcountry-perl (1.33-1) unstable; urgency=low

  * New upstream release.
  * debian/control: New maintainer: Debian Perl Group.
  * debian/rules: Used standard DPG rules file
  * debian/watch: Added to track new upstream versions.

 -- Marc 'HE' Brockschmidt <he@debian.org>  Wed, 25 Feb 2004 17:19:49 +0100

liblocale-subcountry-perl (1.23-3) unstable; urgency=low

  * debian/control: upgraded to Debian Policy 3.6.1 (no changes)

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat,  7 Feb 2004 22:23:15 -0600

liblocale-subcountry-perl (1.23-2) unstable; urgency=low

  * debian/control: upgraded to Debian Policy 3.6.0 (no changes)

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat, 26 Jul 2003 20:10:56 -0500

liblocale-subcountry-perl (1.23-1) unstable; urgency=low

  * New upstream release
  * debian/copyright: updated
  * debian/rules: moved debhelper compatibility level setting to
    'debian/compat' per latest debhelper best practices
  * debian/control: updated sections according to latest archive changes:
    - 'liblocale-subcountry-perl' from 'interpreters' to 'perl'
  * debian/control: upgraded to Debian Policy 3.5.10 (no changes)

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat, 19 Jul 2003 13:12:57 -0500

liblocale-subcountry-perl (1.22-1) unstable; urgency=low

  * New upstream release
  * debian/control: upgraded to Debian Policy 3.5.8 (no changes)

 -- Ardo van Rangelrooij <ardo@debian.org>  Fri, 31 Jan 2003 22:41:38 -0600

liblocale-subcountry-perl (1.11-2) unstable; urgency=low

  * debian/rules: upgraded to debhelper v4
  * debian/control: changed build dependency on debhelper accordingly
  * debian/rules: migrated from 'dh_movefiles' to 'dh_install'
  * debian/rules: split off 'install' target from 'binary-indep' target
  * debian/copyright: added pointer to license

 -- Ardo van Rangelrooij <ardo@debian.org>  Sun,  4 Aug 2002 19:05:36 -0500

liblocale-subcountry-perl (1.11-1) unstable; urgency=low

  * Initial Release

 -- Ardo van Rangelrooij <ardo@debian.org>  Fri, 19 Apr 2002 23:03:10 -0500
